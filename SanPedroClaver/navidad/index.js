var bolas = jQuery('.bolas img');
var velocidad = jQuery('#velocidad');
var variacion = jQuery('#variacion');
var start = jQuery('#start');


function var1() {
    bolas.css("animation-name", "light1");
}

function var2() {
    bolas.css("animation-name", "light2");
    var velLocal = parseFloat(velocidad[0].value) / 2;
    for (var i = 0; i < bolas.length; i++) {
        bolas[i].style.animationDelay = velLocal + "s";
        velLocal = velLocal + parseFloat(velocidad[0].value) / 2;
    }
}

function var3() {
    bolas.css("animation-name", "light3");
    var velLocal = parseFloat(velocidad[0].value) / 4;
    for (var i = 0; i < bolas.length; i++) {
        bolas[i].style.animationDelay = velLocal + "s";
        velLocal = velLocal + parseFloat(velocidad[0].value) / 4;
    }
}

start.click(function () {
    bolas.css({"animation-duration": velocidad.val() + "s", "animation-name": "light1"})
});


let cerrar = document.querySelectorAll(".close")[0];
let abrir = document.querySelectorAll(".bolas")[0];
let modal = document.querySelectorAll(".modal")[0];
let modalC = document.querySelectorAll(".modal-container")[0];

abrir.addEventListener("click", function (e) {
    e.preventDefault();
    modalC.style.opacity = "1";
    modalC.style.visibility = "visible";
    modal.classList.toggle("modal-close");
});

cerrar.addEventListener("click", function(){
  modal.classList.toggle("modal-close");

    setTimeout(function (){
        modalC.style.opacity = "0";
        modalC.style.visibility = "hidden";
    },400)
})

