<?php

define('__MODULO__', 'RolPer');
require('../../Paco/shell/' . __MODULO__ . '/_' . basename(__FILE__));

$Gestor = new _crear_rolper();
?>


<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <title>San Pedro Claver Admin</title>
    <link rel="stylesheet" href="../../../fontawesome/css/all.css">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel="stylesheet" href="../../../css/bootstrap-4.6.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../Paco/js/dist1/sweetalert2.min.css">
    <script type="text/javascript" src="../../Paco/js/dist1/sweetalert2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Paco/CSS/Forms.css">

</head>
<body>
<input type="hidden" id="accion" value="I"/>
<div class="wrapper">
    <div id="formContent">
<!--<table>
    <tr>
        <td class="titulo" colspan="2" align="center"><h1>Detalles Roles</h1></td>
    </tr>

    <tr>
        <td>Descripci&oacute;n del rol:</td>
        <td><input type="text" id="descripcion" value="" size="13" maxlength="50"
                   title="Alfanumérico (9/15)"></td>
    </tr>

    <tr>
        <td>Estado:</td>
        <td>
            <select id="estado">
                <option value='1'>Activo</option>
                <option value='0'>Inactivo</option>
            </select>
        </td>
    </tr>
</table>-->
        <br/>
        <br/>
        <div class="form-group">
            <label>Descripci&oacute;n del rol:</label>
            <input type="text" id="descripcion" value="" size="13" maxlength="50"
                   title="Alfanumérico (9/15)">
        </div>
        <div class="form-group">
            <label>Estado:</label>
            <select id="estado">
                <option value='1'>Activo</option>
                <option value='0'>Inactivo</option>
            </select>
        </div>

<br/>
<input type="button" id="btn" value="Aceptar" class="boton" onclick="datos('I')">
    </div>
</div>
</body>
</html>

