<?php
define('__MODULO__', 'RolPer');
require('../../Paco/shell/' . __MODULO__ . '/_' . basename(__FILE__));


$Gestor = new _rolper_Detalle();

$ROW = $Gestor->ObtieneDatos();

?>


<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <title>San Pedro Claver Admin</title>
    <link rel="stylesheet" href="../../../fontawesome/css/all.css">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel="stylesheet" href="../../../css/bootstrap-4.6.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../Paco/js/dist1/sweetalert2.min.css">
    <script type="text/javascript" src="../../Paco/js/dist1/sweetalert2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Paco/CSS/Forms.css">

</head>
<body>
<input type="hidden" id="accion" value="M"/>
<input type="hidden" id="id_rol" value="<?= $ROW[0]['id_rol'] ?>"/>
<div class="wrapper">
    <div id="formContent">
        <div class="form-group">
            <br/>
            <br/>
   <h1>Detalles Roles</h1>
        </div>

    <div class="form-group">
        <label>Descripci&oacute;n del rol:</label>
        <td><input type="text" id="descripcion" value="<?= $ROW[0]['descripcion'] ?>" size="13" maxlength="50"
                   title="Alfanumérico (9/15)"></td>
    </div>
    <div class="form-group">
        <label>Estado:</label>
            <select id="estado">
                <option value='1'<?= $ROW[0]['estado'] == 1 ? 'selected' : '' ?>>Activo</option>
                <option value='0'<?= $ROW[0]['estado'] == 0 ? 'selected' : '' ?>>Inactivo</option>
            </select>
    </div>

<br/>
<input type="button" id="btn" value="Aceptar" class="boton" onclick="datos('M')">
<input type="button" id="btn" value="Salir" class="boton" onclick="window.close()">
    </div>
</div>
</body>
</html>