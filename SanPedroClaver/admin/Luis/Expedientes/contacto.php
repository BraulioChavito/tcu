<?php
define('__MODULO__', 'Expedientes');
require('../../Paco/shell/' . __MODULO__ . '/_' . basename(__FILE__));
$Gestor = new _contacto();

$ROW = $Gestor->ObtieneDatosPAM();
$ROW1 = $Gestor->ObtieneDatos();
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <title>San Pedro Claver Admin</title>
    <link rel="stylesheet" href="../../../fontawesome/css/all.css">
    <link rel="stylesheet" href="../../Paco/CSS/tablas/bridge.css">
    <link rel="stylesheet" href="../../../css/bootstrap-4.6.1-dist/css/bootstrap.min.css">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel="stylesheet" href="../../../fontawesome/css/all.css">
    <script src="../../Paco/js/sweetalert.min.js"></script>
    <script src="../../Paco/js/jquery-3.6.0.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../../css/bootstrap-4.6.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../SanPedroClaver/css/Galeria.css">
    <script src="../../Paco/CSS/window.css"></script>
    <script src="../../Paco/js/Expedientes/contacto.php.js">
        <!--llama al tablas-->
        <script src="../../Paco/js/dist1/jquery-2.2.3.min.js"></script>
    <script src="../../Paco/js/dist1/fastclick.js"></script>
    <script src="../../Paco/js/dist1/dataTables.bootstrap.min.js"></script>
    <script src="../../Paco/js/dist1/jquery.dataTables.min.js"></script>
    <!--llama al sweet-->
    <link rel="stylesheet" type="text/css" href="../../Paco/js/dist1/sweetalert2.min.css">
    <script type="text/javascript" src="../../Paco/js/dist1/sweetalert2.min.js"></script>
</head>


<body>
<input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
<input type="hidden" id="idPAM" name="idPAM" value="<?= $ROW[0]['id'] ?>"/>
<input type="hidden" id="cedulaPAM" name="cedulaPAM" value="<?= $ROW[0]['cedula'] ?>"/>
<table>
    <tr>
        <td class="titulo" colspan="2" align="center"><h1>Expediente de contacto</h1></td>
    </tr>
    <tr>
        <td class="subTitulo" colspan="2"><h3>Huesped</h3></td>
    </tr>

    <tr>
        <td>C&eacute;dula:</td>
        <td><strong> <?= $ROW[0]['cedula'] ?></strong></td>
    </tr>
    <tr>
        <td>Nombre:</td>
        <td><strong><?= $ROW[0]['nombre'] ?></strong></td>
    </tr>
    <tr>
        <td class="subTitulo" colspan="2"><h3>Datos de contacto</h3></td>
    </tr>
    <tr>
        <td>C&eacute;dula:</td>
        <td><input type="text" id="cedulacontacto" name="cedulacontacto"
                   value="<?= isset($ROW1[0]['cedulaContacto']) == true ? $ROW1[0]['cedulaContacto'] : '' ?>"></td>
    </tr>
    <tr>
        <td>Nombre Completo:</td>
        <td><input type="text" id="nombrecontacto" name="nombrecontacto"
                   value="<?= isset($ROW1[0]['nombreContacto']) == true ? $ROW1[0]['nombreContacto'] : '' ?>"></td>
    </tr>
    <tr>
        <td>Direcci&oacute;n</td>
        <td><input type="text" name="direccioncontacto" id="direccioncontacto"
                   value="<?= isset($ROW1[0]['direccionContacto']) == true ? $ROW1[0]['direccionContacto'] : '' ?>">
        </td>
    </tr>
    <tr>
        <td>Parentezco</td>
        <td><input type="text" name="parentesco" id="parentesco"
                   value="<?= isset($ROW1[0]['parentesco']) == true ? $ROW1[0]['parentesco'] : '' ?>"></td>
    </tr>

    <tr>
        <td>Ocupaci&oacute;n</td>
        <td><input type="text" name="ocupacion" id="ocupacion"
                   value="<?= isset($ROW1[0]['ocupacion']) == true ? $ROW1[0]['ocupacion'] : '' ?>"></td>
    </tr>
    <tr>
        <td>Email</e></td>
        <td><input type="text" name="email" id="email"
                   value="<?= isset($ROW1[0]['email']) == true ? $ROW1[0]['email'] : '' ?>"></td>
    </tr>
    <tr>
        <td>Telef&oacute;no</e></td>
        <td><input type="text" name="telefono" id="telefono"
                   value="<?= isset($ROW1[0]['telefono']) == true ? $ROW1[0]['telefono'] : '' ?>"></td>
    </tr>
</table>
<br/>
<?php
if (isset($ROW1[0]['cedulaContacto']) == false) {
    ?>
    <input type="button" id="btn" value="Agregar" class="boton" onclick="datos('I')">
<?php } else { ?>
    <input type="button" id="btn" value="Modificar" class="boton" onclick="datos('M')">
<?php } ?>
<input type="button" id="btn" value="Salir" class="boton" onclick="window.close()">

</body>

</html>
