<?php
define('__MODULO__', 'Expedientes');
require('../../Paco/shell/' . __MODULO__ . '/_' . basename(__FILE__));

$Gestor = new _expediente_detalle();
$user = $Gestor->Get('UID');
$rol = $Gestor->rol($user);
if ($rol == 1) {
    $desRol = 'Administrador';
} elseif ($rol == 2) {
    $desRol = 'Usuarios';
}


$ROW = $Gestor->ObtieneDatos();


?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <title>San Pedro Claver Admin</title>
    <link rel="stylesheet" href="../../../fontawesome/css/all.css">
    <link rel="stylesheet" href="../../Paco/CSS/tablas/bridge.css">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel="stylesheet" href="../../../css/bootstrap-4.6.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../Paco/js/dist1/sweetalert2.min.css">
    <script type="text/javascript" src="../../Paco/js/dist1/sweetalert2.min.js"></script>


    <script src="../../Paco/js/dist1/bootstrap-datetimepicker.js"></script>
    <script src="../../Paco/js/dist1/bootstrap-datepicker.js"></script>
    <script src="../../Paco/js/dist1/bootstrap-timepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Paco/CSS/Forms.css">
</head>

<body>
<div class="wrapper">
    <div id="formContent">
<br/>
        <div class="form-group">
            <h1>Detalle Huesped</h1>
        </div>
        <div class="form-group">
            <h3>Datos personales</h3>
        </div>

        <div class="form-group">
            <label>Cédula:</label>
      <input type="text" id="cedula" value="<?= $ROW[0]['cedula'] ?>" readonly>
        </div>
        <div class="form-group">
            <label>Nombre:</label>
        <input type="text" id="nombre" value="<?= $ROW[0]['nombre'] ?>" >
        </div>
        <div class="form-group">
            <label>Fecha Nacimiento:</label>

           <input  type="date" class="form-control" id="fechaNacimiento" name="fechaNacimiento" value="<?= $ROW[0]['fechaNacimiento'] ?>">
        </div>
        <div class="form-group">
            <label>Nacionalidad:</label>
        <input type="text" id="nacionalidad"  name="nacionalidad" value="<?= $ROW[0]['nacionalidad'] ?>" >
        </div>
        <div class="form-group">
            <label>Estado Civil:</label>

            <select id="estadoCivil">
                <option value='1'<?= $ROW[0]['estadocivil'] == 1 ? 'selected' : '' ?>>Persona Soltera</option>
                <option value='2'<?= $ROW[0]['estadocivil'] == 2 ? 'selected' : '' ?>>Persona Casada</option>
                <option value='3'<?= $ROW[0]['estadocivil'] == 3 ? 'selected' : '' ?>>Persona en Union Libre</option>
                <option value='4'<?= $ROW[0]['estadocivil'] == 4 ? 'selected' : '' ?>>Persona Viuda</option>
            </select>

        </div>

        <div class="form-group">
            <label>Escolaridad:</label>

            <select id="escolaridad">
                <option value='1'<?= $ROW[0]['escolaridad'] == 1 ? 'selected' : '' ?>>Ninguna</option>
                <option value='2'<?= $ROW[0]['escolaridad'] == 2 ? 'selected' : '' ?>>Primaria Incompleta</option>
                <option value='3'<?= $ROW[0]['escolaridad'] == 3 ? 'selected' : '' ?>>Primaria Completa</option>
                <option value='4'<?= $ROW[0]['escolaridad'] == 4 ? 'selected' : '' ?>>Secundaria Incompleta</option>
                <option value='5'<?= $ROW[0]['escolaridad'] == 5 ? 'selected' : '' ?>>Secundaria Completa</option>
                <option value='6'<?= $ROW[0]['escolaridad'] == 6 ? 'selected' : '' ?>>Universitaria Incompleta</option>
                <option value='7'<?= $ROW[0]['escolaridad'] == 7 ? 'selected' : '' ?>>Universitaria Completa</option>
                <option value='8'<?= $ROW[0]['escolaridad'] == 8 ? 'selected' : '' ?>>Posgrado</option>
                <option value='9'<?= $ROW[0]['escolaridad'] == 9 ? 'selected' : '' ?>>Otro</option>

            </select>

        </div>

        <div class="form-group">
            <label>Ocupaci&oacute;n:</label>
       <input type="text" name="ocupacion" id="ocupacion" value="<?= $ROW[0]['ocupacion'] ?>" >
        </div>

        <div class="form-group">
            <label>Direcci&oacute;n:</label>
     <input type="text" name="direccion" id="direccion" value="<?= $ROW[0]['direccion'] ?>" >
        </div>

        <div class="form-group">
            <label>Fecha Ingreso:</label>
     <input  type="date" class="form-control" id="fechaIngreso" name="fechaIngreso" value="<?= $ROW[0]['fechaingreso'] ?>">
        </div>
        <div class="form-group">
            <label>Estado:</label>
            <select id="estado">
                <option value='1'<?= $ROW[0]['estado'] == 1 ? 'selected' : '' ?>>Activo</option>
                <option value='0'<?= $ROW[0]['estado'] == 0 ? 'selected' : '' ?>>Inactivo</option>
            </select>

        </div>

<br/>

<input type="button" id="btn" value="Modificar" class="boton" onclick="datos('M')">
<input type="button" id="btn" value="Salir" class="boton" onclick="window.close()">
    </div>
</div>

</body>








</html>