<?php
define('__MODULO__', 'Expedientes');
require('../../Paco/shell/' . __MODULO__ . '/_' . basename(__FILE__));

$Gestor = new _Expediente();
$user = $Gestor->Get('UID');

$rol = $Gestor->rol($user);
if ($rol == 1) {
    $desRol = 'Administrador';
} elseif ($rol == 2) {
    $desRol = 'Usuarios';
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <title>San Pedro Claver Admin</title>
    <link rel="stylesheet" href="../../../fontawesome/css/all.css">
    <link rel="stylesheet" href="../../Paco/CSS/tablas/bridge.css">
    <link rel="stylesheet" href="../../../css/bootstrap-4.6.1-dist/css/bootstrap.min.css">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="stylesheet" href="../../../fontawesome/css/all.css">
    <script src="../../Paco/js/sweetalert.min.js"></script>
    <script src="../../Paco/js/jquery-3.6.0.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../../css/bootstrap-4.6.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../SanPedroClaver/css/Galeria.css">
    <script src="../../Paco/CSS/window.css"></script>
    <script src="../../Paco/js/Expedientes/expediente.php.js"></script>
    <script src="../../Paco/js/dist1/jquery-2.2.3.min.js"></script>
    <script src="../../Paco/js/dist1/fastclick.js"></script>
    <script src="../../Paco/js/dist1/dataTables.bootstrap.min.js"></script>
    <script src="../../Paco/js/dist1/jquery.dataTables.min.js"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../Paco/CSS/Expediente.css">
</head>

<div>
    <table rules="none" width="98%">
        <tr>
            <td rowspan="3" style="width: 65%"><a href="#"><img src="../../../images/miniLogo.png"> </a></td>
        </tr>
        <tr>
            <td>
                Usuario:
            </td>
            <td>
                <?= $Gestor->Get('UNAME') ?>
            </td>
            <td rowspan="2">
                <button class="btn" title="Cerrar Sesión" >
                    <img src="../../../images/Utilitarios/icon/Exit.ico" style="height: 60px; width: 60px">
                </button>
            </td>
        </tr>
        <tr>
            <td>
                Rol:
            </td>
            <td>
                <?= $desRol ?>
            </td>
        </tr>
    </table>
</div>

<?php
if ($rol == 1) {
    $cuerpo = '<body>
<nav class="navbar navbar-expand-lg navbar-light bg-info"> 
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link"  href="../Seguridad/principal.php">Inicio <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Huespedes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Habitaciones</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">Inventarios</a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="#">Personal</a>
      </li>
         <li class="nav-item">
        <a class="nav-link" href="#">Reportes</a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="Usuarios.php">Seguridad</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="../Usuarios/Usuarios.php" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Usuarios
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Agregar Nuevo Usuario</a>
          <a class="dropdown-item" href="../Usuarios/Usuarios.php">Mostrar Usuarios</a>
          <a class="dropdown-item" href="#">Eliminar Usuarios</a>
        </div>
      </li>
    </ul>
  </div> 
</nav>

    </div>
';
    echo $cuerpo;
} else {
    echo '<h1>Sin permiso</h1>
';
}
?>



<section class="content">
    <div class="box">
        <div class="padre" >

<div class="hijo">
    <br/>
    <h3> Expedientes Registrados</h3></div>

        </div>

        <div class="box-body">
            <script>
                $(function () {
                    $('#expedientes').DataTable();
                });
            </script>
            <table id="expedientes" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>C&eacute;dula</th>
                    <th>Nombre</th>
                    <th>Estado</th>
                    <th>Acciones</th>

                </tr>
                </thead>
                <tbody>
                <?php
                $ROW = $Gestor->DatosMuestra();

                for ($x = 0; $x < count($ROW); $x++) {

                    ?>
                    <tr>
                        <td><a href="#"
                               onclick="expedientesModifica('<?= $ROW[$x]['id'] ?>')"><?= $ROW[$x]['cedula'] ?></a>
                        </td>
                        <td><?= $ROW[$x]['nombre'] ?></td>
                        <td><?= $ROW[$x]['estado'] == 1 ? 'Activo' : 'Inactivo' ?></td>
                        <td align="center">
                            <button><img src="<?php $Gestor->Incluir('contacto', 'bkg') ?>" class="tab3" title="Datos de Contacto" onclick="contacto('<?= $ROW[$x]['id'] ?>')"/></button> &nbsp;&nbsp;&nbsp;
                            <button><img src="<?php $Gestor->Incluir('medico', 'bkg') ?>" class="tab3" title="Expediente Medico" onclick="medico('<?= $ROW[$x]['id'] ?>')"/></button>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <div class="padre" >

                <div class="hijo">
                    <button class="btn" title="Agregar nuevo expediente" onclick="expedienteAgrega()" STYLE="align-content: center" >
                        <img src="../../../images/Utilitarios/icon/Agregar.jpg" style="height: 80px; width: 80px">
                    </button>
                </div>
            </div>
        </div>
        <br/>
    </div>
</section>
<div class="padre" >

    <div class="hijo">
<footer class="Pfooter">
    <strong>&copy; <?= date('Y') ?>  </strong>
</footer>
    </div>
</div>
</body>

</html>