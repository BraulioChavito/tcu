<?php
define('__MODULO__', 'Expedientes');
require('../../Paco/shell/' . __MODULO__ . '/_' . basename(__FILE__));

$Gestor = new _expediente_agregar();


?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <title>San Pedro Claver Admin</title>
    <link rel="stylesheet" href="../../../fontawesome/css/all.css">
    <link rel="stylesheet" href="../../Paco/CSS/tablas/bridge.css">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel="stylesheet" href="../../../css/bootstrap-4.6.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../Paco/js/dist1/sweetalert2.min.css">
    <script type="text/javascript" src="../../Paco/js/dist1/sweetalert2.min.js"></script>


    <script src="../../Paco/js/dist1/bootstrap-datetimepicker.js"></script>
    <script src="../../Paco/js/dist1/bootstrap-datepicker.js"></script>
    <script src="../../Paco/js/dist1/bootstrap-timepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Paco/CSS/Forms.css">
</head>

<body>
<input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
<div class="wrapper">
    <div id="formContent">

    <div class="form-group">
       <h1>Agregar Huesped</h1>
    </div>
    <div class="form-group">
        <h3>Datos personales</h3>
    </div>

    <div class="form-group">
        <label>Cédula:</label>
      <input type="text" id="cedula" value="">
    </div>
    <div class="form-group">
        <label>Nombre:</label>
 <input type="text" id="nombre" >
    </div>
<div class="form-group">
    <label>Fecha Nacimiento:</label>

    <input  type="date" class="form-control" id="fechaNacimiento" name="fechaNacimiento">
    </div>
    <div class="form-group">
        <label>Nacionalidad:</label>

       <input type="text" id="nacionalidad"  name="nacionalidad" value="" >
    </div>
        <div class="form-group">
            <label>Estado Civil:</label>
            <select id="estadoCivil">
                <option value='0'> --Seleccione--</option>
                <option value='1'> Persona Soltera</option>
                <option value='2'>Persona Casada</option>
                <option value='3'>Persona en Union Libre</option>
                <option value='4'>Persona Viuda</option>
            </select>
        </div>

    <div class="form-group">
        <label>Escolaridad:</label>
            <select id="escolaridad">
                <option value='0'> --Seleccione--</option>
                <option value='1'> Ninguna</option>
                <option value='2'>Primaria Incompleta</option>
                <option value='3'>Primaria Completa</option>
                <option value='4'>Secundaria Incompleta</option>
                <option value='5'>Secundaria Completa</option>
                <option value='6'>Universitaria Incompleta</option>
                <option value='7'>Universitaria Completa</option>
                <option value='8'>Posgrado</option>
                <option value='9'>Otro</option>
            </select>
    </div>

    <div class="form-group">
        <label>Ocupaci&oacute;n:</label>
      <input type="text" name="ocupacion" id="ocupacion" value="" >
    </div>

    <div class="form-group">
        <label>Direcci&oacute;n:</label>
       <input type="text" name="direccion" id="direccion" value="" >
    </div>

    <div class="form-group">
        <label>Fecha Ingreso:</label>
       <input  type="date" class="form-control" id="fechaIngreso" name="fechaIngreso">
    </div>
    <div class="form-group">
        <label>Estado:</label>

            <select id="estado">
                <option value='1'>Activo</option>
                <option value='0'>Inactivo</option>
            </select>
    </div>
<br/>
<input type="button" id="btn" value="Aceptar" class="boton" onclick="datos('I')">
    </div>
</div>


</body>



</html>