<?php
define('__MODULO__', 'Usuarios');
require('../../Paco/shell/' . __MODULO__ . '/_' . basename(__FILE__));

$Gestor = new _Usuario_Detalle();
$user = $Gestor->Get('UID');
$rol = $Gestor->rol($user);
if ($rol == 1) {
    $desRol = 'Administrador';
} elseif ($rol == 2) {
    $desRol = 'Usuarios';
}

$ROW = $Gestor->ObtieneDatos();
//$ROW1 = $Gestor->ObtieneRol();

if (!$ROW) die('Usuario inexistente');

?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <title>San Pedro Claver Admin</title>
    <link rel="stylesheet" href="../../../fontawesome/css/all.css">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel="stylesheet" href="../../../css/bootstrap-4.6.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../Paco/js/dist1/sweetalert2.min.css">
    <script type="text/javascript" src="../../Paco/js/dist1/sweetalert2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Paco/CSS/Forms.css">

</head>
<body>
<input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>
<input type="hidden" id="id" value="<?= $ROW[0]['id'] ?>"/>
<div class="wrapper">
    <div id="formContent">

<table>
    <tr>
        <td class="titulo" colspan="2" align="center"><h1>Detalles Usuario</h1></td>
    </tr>
    <tr>
        <td class="subTitulo" colspan="2"><h3>Datos personales</h3></td>
    </tr>

    <tr>
        <td>C&eacute;dula:</td>
        <td><input type="text" id="cedula" value="<?= $ROW[0]['cedula'] ?>" size="13" maxlength="15"
                   title="Alfanumérico (9/15)"></td>
    </tr>
    <tr>
        <td>Nombre:</td>
        <td><input type="text" id="nombre" value="<?= $ROW[0]['nombre'] ?>" size="30" maxlength="30"
                   title="Alfanumérico (2/30)"></td>
    </tr>
    <tr>
        <td>Correo:</td>
        <td><input type="email" name="email" id="correo" value="<?= $ROW[0]['correo'] ?>" size="30" maxlength="30"
                   title="Alfanumérico (4/30)"></td>
    </tr>
    <tr>
        <td>Tel&eacute;fono:</td>
        <td><input type="text" id="telefono" value="<?= $ROW[0]['telefono'] ?>" size="30" maxlength="30"
                   title="Alfanumérico (4/30)"></td>
    </tr>
    <tr>
        <td>Contrase&ntilde;a:</td>
        <td><input type="text" id="clave" value="<?= $ROW[0]['clave'] ?>" size="30" maxlength="30"
                   title="Alfanumérico (4/30)"></td>
    </tr>

    <tr>
        <td colspan="2">
            <hr/>
        </td>
    </tr>
    <tr>
        <td class="subTitulo" colspan="2"><h3>Tipo de Usuario</h3></td>
    </tr>

    <tr>
        <td>Rol</td>
        <?php if ($ROW[0]['cedula'] == '') { ?>
            <td>
                <select id="listaRoles">
                    <option value="">...</option>
                    <?php
                    $ROWF = $Gestor->ListaRoles();
                    for ($x = 0; $x < count($ROWF); $x++) {
                        echo "<option value='{$ROWF[$x]['id']}' label='{$ROWF[$x]['nombre']}' label='{$ROWF[$x]['nombre']}'>{$ROWF[$x]['nombre']}</option>";
                    } ?>
                </select>
            </td>
        <?php } else { ?>
            <td>
                <select id="listaRoles">
                    <?php foreach ($ROW as $dato) { ?>
                        <option value="<?= $dato['rol'] ?>"><?= $dato['rol'] ?></option>
                    <?php } ?>
                </select>
            </td>
        <?php } ?>
    </tr>
    <tr>
        <td>Estado:</td>
        <td>
            <select id="estado">
                <option value='1'<?= $ROW[0]['estado'] == 1 ? 'selected' : '' ?>>Activo</option>
                <option value='0'<?= $ROW[0]['estado'] == 0 ? 'selected' : '' ?>>Inactivo</option>
            </select>
        </td>
    </tr>
</table>

<br/>
<input type="button" id="btn" value="Aceptar" class="boton" onclick="datos('M')">
</div>
</div>
</body>