<?php
define('__MODULO__', 'Usuarios');
require('../../Paco/shell/' . __MODULO__ . '/_' . basename(__FILE__));

$Gestor = new _usuario_agregar();
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <title>San Pedro Claver Admin</title>
    <link rel="stylesheet" href="../../../fontawesome/css/all.css">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <link rel="stylesheet" href="../../../css/bootstrap-4.6.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../Paco/js/dist1/sweetalert2.min.css">
    <script type="text/javascript" src="../../Paco/js/dist1/sweetalert2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Paco/CSS/Forms.css">

</head>
<body>
<!--<input type="hidden" id="accion" value="<?= $_GET['acc'] ?>"/>-->
<div class="wrapper">
    <div id="formContent">
        <div class="form-group">
            <h1>Agregar Usuario</h1>
        </div>
        <div class="form-group">
            <h3>Datos personales</h3>
        </div>

        <div class="form-group">
            <label>Cédula:</label>
        <input type="text" id="cedula" value="" size="13" maxlength="15"
                   title="Alfanumérico (9/15)">
        </div>
        <div class="form-group">
            <label>Nombre:</label>
      <input type="text" id="nombre" value="" size="30" maxlength="30"
                   title="Alfanumérico (2/30)">
        </div>
        <div class="form-group">
            <label>Correo:</label>
        <input type="email" name="email" id="correo" value="" size="30" maxlength="30"
                   title="Alfanumérico (4/30)">
        </div>
        <div class="form-group">
            <label>Télefono:</label>
        <input type="text" id="telefono" value="" size="30" maxlength="30"
                   title="Alfanumérico (4/30)">
        </div>
        <div class="form-group">
            <label>Contraseña:</label>
        <input type="text" id="clave" value="" size="30" maxlength="30"
                   title="Alfanumérico (4/30)">

            <br/>
            <div class="form-group">
                <h3>Tipo de usuario</h3>
            </div>

            <div class="form-group">
                <label>Rol:</label>
            <select id="id_rol">
                <option value="">...</option>
                <?php
                $ROWF = $Gestor->ListaRoles();
                for ($x = 0; $x < count($ROWF); $x++) {
                    echo "<option value='{$ROWF[$x]['id_rol']}' label='{$ROWF[$x]['nombre']}' label='{$ROWF[$x]['nombre']}'>{$ROWF[$x]['nombre']}</option>";
                } ?>
            </select>
            </div>

            <div class="form-group">
                <label>Estado:</label>
            <select id="estado">
                <option value='1'>Activo</option>
                <option value='0'>Inactivo</option>
            </select>
            </div>
<br/>
<input type="button" id="btn" value="Agregar" class="boton" onclick="datos('I')">
    </div>
</div>
</body>
</html>