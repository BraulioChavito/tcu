<?php

use Seguridad\_principal;

define('__MODULO__', 'Seguridad');
require('../../Paco/shell/' . __MODULO__ . '/_' . basename(__FILE__));

$Gestor = new _principal();

$user = $Gestor->Get('UID');
$rol = $Gestor->rol($user);
if ($rol == 1) {
    $desRol = 'Administrador';
} elseif ($rol == 2) {
    $desRol = 'Usuarios';
}



?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <title>San Pedro Claver Admin</title>
    <link rel="stylesheet" href="../../../fontawesome/css/all.css">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <script src="../../Paco/js/sweetalert.min.js"></script>
    <script src="../../Paco/js/jquery-3.6.0.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../../css/bootstrap-4.6.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../">
    <script src="../../../css/bootstrap-4.6.1-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Paco/CSS/Login.css">
    <link rel="stylesheet" type="text/css" href="../../Paco/CSS/tablas/General.css">
</head>

<div>
    <table rules="none" width="98%">
        <tr>
            <td rowspan="3" style="width: 65%"><a href="#"><img src="../../../images/miniLogo.png"> </a></td>
        </tr>
        <tr>
            <td>
                Usuario:
            </td>
            <td>
                <?= $Gestor->Get('UNAME') ?>
            </td>
            <td rowspan="2">
                <button class="btn" title="Cerrar Sesión" >
                    <img src="../../../images/Utilitarios/icon/Exit.ico" style="height: 60px; width: 60px">
                </button>
            </td>
        </tr>
        <tr>
            <td>
                Rol:
            </td>
            <td>
                <?= $desRol ?>
            </td>
        </tr>
    </table>
</div>
<br/>

<?php
if ($rol == 1) {
    echo
    ' <body>
<nav class="navbar navbar-expand-lg navbar-light bg-info"> 
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="principal.php">Inicio <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../Expedientes/expediente.php">Expedientes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Habitaciones</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="#">Inventarios</a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="#">Personal</a>
      </li>
         <li class="nav-item">
        <a class="nav-link" href="#">Reportes</a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="../Usuarios/Usuarios.php">Seguridad</a>
      </li>
         <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="../Usuarios/Usuarios.php" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Usuarios
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="../Usuarios/usuario_agregar.php">Agregar Nuevo Usuario</a>
          <a class="dropdown-item" href="../Usuarios/Usuarios.php">Mostrar Usuarios</a>
          <a class="dropdown-item" href="#">Eliminar Usuarios</a>
        </div>
      </li>      
      </li>
         <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="../Usuarios/Usuarios.php" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Roles y Permisos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="../RolPer/rolper.php">Creación de roles y permisos</a>
          <a class="dropdown-item" href="../RolPer/rolper.php">Mostrar roles y permisos</a>         
        </div>
      </li>
      
      
      
      
    </ul>
  </div> 
</nav>

<div style="background-size: 100% 100%; height: 600px; width: 90%  background-repeat: no-repeat; background-image: url(../../../images/Entrada.jpg)" >

</div>



</body>
';
} else {
    echo '<h1>Sin permiso</h1>
</body>';
}
?>

<!--
<hr>
<footer class="Pfooter">
    <strong>&copy; <?= date('Y') ?>  </strong>
</footer>-->


</html>
