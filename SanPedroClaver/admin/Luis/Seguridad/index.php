<?php

use Seguridad\_index;

define('__MODULO__', 'Seguridad');

require ('../../Paco/shell/'.__MODULO__.'/_'.basename(__FILE__));


$Gestor = new _index();


?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <link rel="stylesheet" href="../../../css/bootstrap-4.6.1-dist/css/bootstrap.min.css">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title><?= $Gestor->Title() ?></title>
    <?php $Gestor->Incluir('window', 'js'); ?>
    <?php $Gestor->Incluir(__MODULO__, 'ajax', basename(__FILE__)); ?>
    <script src="../../Paco/js/Seguridad/index.php.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Paco/js/dist1/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="../../Paco/CSS/Login.css">
    <script type="text/javascript" src="../../Paco/js/dist1/sweetalert2.all.min.js"></script>



</head>
<body>


      <!--  <form name="login" id="login" method="post" action="index.php" onsubmit="return login.login()">
            <center>
                <div class="row">
                    <img src="../../../images/Utilitarios/icon/User1.ico" class="img-rounded" alt="logo">
                    <div class="box box-info">
                        <br/>
                        <br/>

                        <div class="box-body">
                            <table style="width:100%" cellpadding="10" cellspacing ="2" >
                                <tr>
                                    <td align="center"><label for="usuario" >Usuario:</label></td>
                                    <td><input type="text" class="form-control" placeholder="Usuario" name="usuario" id="usuario" required autofocus></td>
                                </tr>
                                <tr>
                                    <td align="center"><label for="clave" >Contraseña:</label></td>
                                    <td> <input class="form-control" placeholder="Contrase&ntilde;a"  type="password" name="clave" id="clave" required/></td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <br/>
                        <br/>
                        <div class="box-footer">
                            <div class="pull-right">
                                <input type="button" class="btn btn-fill btn-info btn-wd" value="Ingresar" onclick="revisar()">
                               <input type="submit" class="btn btn-fill btn-info btn-wd" value="Ingresar">
                            </div>
                        </div>
                    </div>
                </div>
            </center>
        </form>-->
      <div class="wrapper">
            <div id="formContent">

                <h2 class="active"> Iniciar Sesión </h2>


                <div class="fadeIn first">
                    <img src="../../../images/Utilitarios/icon/User1.ico" class="img-rounded" alt="logo" style="width: 170px; height: 170px">
                </div>


                <form>
                    <input type="text" class="form-control" placeholder="Usuario" name="usuario" id="usuario" required autofocus>
                    <br/>
                    <input class="form-control" placeholder="Contrase&ntilde;a"  type="password" name="clave" id="clave" required/>
                    <br/>
                    <input type="button"  value="Ingresar" onclick="revisar()">
                </form>

            </div>
        </div>

    <hr>
    <footer class="Pfooter">
        <strong>&copy; <?=date('Y')?>  </strong>
    </footer>

</body>
</html>
