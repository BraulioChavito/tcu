<?php

/* * *****************************************************************
  CLASE DE NIVEL 1 ENCARGADA DEL MODULO DE SEGURIDAD
 * ***************************************************************** */

namespace Hugo;

use function setcookie;

final class Seguridad extends Database
{

    private function SesionInit()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    function getRol($_usuario)
    {
        $Q = $this->_QUERY("SELECT rol FROM usuario_rol WHERE usuario = '{$_usuario}';");
        return $Q[0]['rol'];
    }


    private function SesionSet($_uid, $_uced, $_unom, $_correo, $_tk)
    {
        $this->SesionInit();
        $_SESSION['_SPC_'] = $_tk;
        $_SESSION['_UID_'] = $_uid;
        $_SESSION['_UCED_'] = $_uced;
        $_SESSION['_UNAME_'] = $_unom;
        $_SESSION['_ROL_'] = '';
        $_SESSION['_PRY_'] = '';
        $_SESSION['_PRL_'] = '';
        $_SESSION['_UMAIL_'] = $_correo;
    }

    function SesionGet()
    {
        $this->SesionInit();
        if (isset($_SESSION['_SPC_'])) {
            return array('SPC' => $_SESSION['_SPC_'], 'UID' => $_SESSION['_UID_'], 'UCED' => $_SESSION['_UCED_'], 'UNAME' => $_SESSION['_UNAME_'], 'ROL' => $_SESSION['_ROL_'], 'PRY' => $_SESSION['_PRY_'], 'PRL' => $_SESSION['_PRL_'], 'UMAIL' => $_SESSION['_UMAIL_']);
        } else {
            return array();
        }
    }

    function SesionDestruye()
    {
        $this->SesionInit();
        $_SESSION = array();
        if (isset($_COOKIE[session_name()]))
            setcookie(session_name(), '', time() - 42000, '/');
        session_destroy();
    }

    function SesionAuth()
    {
        $this->SesionInit();
        if (!isset($_SESSION['_UID_']) or $_SESSION['_UID_'] === '') {
            return false;
        }
        return true;
    }

    function UsuarioFin()
    {
        $this->SesionInit();
        $this->Logger('Fin de sesion');
        $this->_TRANS("DELETE FROM segsesiones WHERE usuario = '{$_SESSION['_UID_']}';");
        $this->SesionDestruye();
    }

    function UsuarioLogin($_login, $_clave)
    {
        if ($_login == '' || $_clave == '') {
            return -2;
        }

        $_clave = base64_encode($_clave);
        $_login = str_replace('-', '', $_login);
        $ROW = $this->_QUERY("SELECT id, cedula, nombre, correo, estado FROM segusuarios WHERE cedula = '{$_login}' AND clave = '{$_clave}' AND estado = '1';");

        if (!$ROW) {
            return -3;
        }

        if ($ROW[0]['estado'] == '0') {
            return -4;
        }

        $tk = date('dmY') . $this->Rand();
        // $this->_TRANS("INSERT INTO segsesiones VALUES('{$tk}', '{$ROW[0]['id']}', NOW());");
        $this->SesionSet($ROW[0]['id'], $ROW[0]['cedula'], $ROW[0]['nombre'], $ROW[0]['correo'], $tk);
        return 1;
    }

    function UsuarioPerfiles($_usuario)
    {
        return $this->_QUERY("SELECT administrativo, proyectista FROM segperfiles WHERE usuario = '{$_usuario}';");
    }

    function UsuarioPerfil($_usuario, $_perfil)
    {
        $perfiles = array('administrativo', 'proyectista');
        $ROW = $this->_QUERY("SELECT {$perfiles[$_perfil - 1]} FROM segperfiles WHERE usuario = '{$_usuario}';");
        if ($ROW[0][$perfiles[$_perfil - 1]] == '1') {
            $this->SesionPerfil($_perfil);
            return 1;
        } else {
            return -3;
        }
    }

    private function SesionPerfil($_perfil)
    {
        $this->SesionInit();
        $_SESSION['_PRL_'] = $_perfil;
    }

    function SesionValida($_sesion, $_usuario)
    {
        $this->SesionInit();
        if (!isset($_SESSION['_SPC_']) or $_SESSION['_SPC_'] === '') {
            return 0;
        } else {
            return 1;
        }
    }

    function UsuarioObtenerDatos($_usuario = '', $_cedula = '', $_nombre = '')
    {
        $str = "SELECT id, cedula, nombre, correo, correoopc, telefono, clave FROM segusuarios WHERE 1=1";
        if ($_usuario != '') {
            $str .= " AND id = '{$_usuario}'";
        }
        if ($_cedula != '') {
            $str .= " AND cedula = '{$_cedula}'";
        }
        if ($_nombre != '') {
            $str .= " AND nombre like '%{$_nombre}%'";
        }
        return $this->_QUERY($str);
    }

    function obtieneProyectosUsuario($_usuario)
    {
        return $this->_QUERY("SELECT segproyectos.codigo, segproyectos.nombre, segrolesusuario.rol FROM segrolesusuario INNER JOIN segproyectos ON segrolesusuario.proyecto = segproyectos.codigo WHERE segrolesusuario.usuario = '{$_usuario}' ORDER BY segproyectos.codigo;");
    }

    function establecerProyectoSesion($_usuario, $_proyecto)
    {
        if ($_proyecto == '') {
            return 0;
        }
        $this->SesionInit();
        $ROW = $this->_QUERY("SELECT rol FROM segrolesusuario WHERE usuario = '{$_usuario}' AND proyecto = '{$_proyecto}';");
        $_SESSION['_PRY_'] = $_proyecto;
        $_SESSION['_ROL_'] = $ROW[0]['rol'];
        return 1;
    }

    function ObtieneNombreUsuarioSesion($_nombre)
    {
        $unombre = explode(' ', $_nombre);
        if (count($unombre) > 3) {
            return $unombre[0] . ' ' . $unombre[2] . ' ' . $unombre[3][0] . '.';
        } elseif (count($unombre) > 2) {
            return $unombre[0] . ' ' . $unombre[1] . ' ' . $unombre[2][0] . '.';
        } elseif (count($unombre) > 1) {
            return $unombre[0] . ' ' . $unombre[1] . '.';
        } else {
            return $unombre;
        }
    }

    function UsuarioDatosActualiza($_correo, $_correoopc, $_telefono, $_clave, $_uid)
    {
        $str = "UPDATE segusuarios SET correo = '{$_correo}', correoopc = '{$_correoopc}', telefono = '{$_telefono}' WHERE id = '{$_uid}'";
        if ($_clave != '') {
            $_clave = base64_encode($_clave);
            $str = "UPDATE segusuarios SET correo = '{$_correo}', correoopc = '{$_correoopc}', telefono = '{$_telefono}', clave = '{$_clave}' WHERE id = '{$_uid}'";
        }
        if ($this->_TRANS($str)) {
            return 1;
        } else {
            return 0;
        }
    }


    /*     * ******************************
     * ******************************
     * *****************************
     * ***************************
     */

    function UsuarioReLogin($_login)
    {
        $tk = date('dmY') . $this->Rand();
        $login = str_replace('-', '', $_login);
        $ROW = $this->_QUERY("SELECT id, cedula, nombre, correo FROM segusuarios WHERE cedula = '{$login}';");
        $this->_TRANS("DELETE FROM segsesiones WHERE usuario = '{$ROW[0]['id']}';");
        $this->_TRANS("INSERT INTO segsesiones VALUES('{$tk}', '{$ROW[0]['id']}', NOW());");
        $this->SesionSet($ROW[0]['id'], $ROW[0]['cedula'], $ROW[0]['nombre'], $ROW[0]['correo'], $tk);
        $this->Logger('Cierre remoto de sesiones e inicio de sesion. Datos: ' . $this->getInformacionMaquinaUsuario() . ' .IP: ' . $this->getIP());
        return 1;
    }

    function opcionesMenu_get($_id)
    {
        return $this->_QUERY("SELECT id, direccion FROM segmenu WHERE id='{$_id}'");
    }

    /* lolo */


    /* lolo */

    private function SesionRol($_rol)
    {
        $this->SesionInit();
        $_SESSION['_ROL_'] = $_rol;
    }

    /* lolo */

    function UsuarioClave($_actual, $_nueva)
    {
        $_actual = base64_encode($_actual);
        $_nueva = base64_encode($_nueva);

        $ROW = $this->_QUERY("SELECT 1 FROM seg001 WHERE (id='{$_SESSION['_UID_']}') AND (clave='{$_actual}');");
        if (!$ROW)
            return false;
        return $this->_TRANS("UPDATE seg001 SET clave='{$_nueva}' WHERE (id='{$_SESSION['_UID_']}');");
    }

    /* lolo */


    /* lolo */


    function usuarioSimple_IME($_sigla, $_id, $_cedula, $_nombre, $_apellido1, $_apellido2, $_correo, $_clave, $_tipo, $_estado)
    {
        $transacciones = array();
        if ($_sigla == 'I') {

        } elseif ($_sigla == 'M') {
            return $this->_TRANS("UPDATE seg001 SET cedula='{$_cedula}',nombre='{$_nombre}',apellido1='{$_apellido1}',apellido2='{$_apellido2}',correo='{$_correo}',clave='{$_clave}',tipo='{$_tipo}',estado='{$_estado}' WHERE id='{$_id}'");
        }
    }

    function usuario_Clave($_id, $_clave)
    {
        return $this->_TRANS("UPDATE seg001 SET clave='{$_clave}' WHERE id='{$_id}'");
    }

    function usuarioGetCedula($_cedula)
    {
        return $this->_QUERY("SELECT id, cedula, nombre, apellido1, apellido2, correo, clave FROM seg001 WHERE cedula = '{$_cedula}';");
    }

    function usuarioGetId($_id)
    {
        return $this->_QUERY("SELECT id, cedula, nombre, apellido1, apellido2, correo, clave FROM seg001 WHERE id = '{$_id}';");
    }

    /**
     * @return int
     * @author Josue Loria <jloriag@hotmail.com>
     */
    function usuarioROL_IME($_sigla, $_id, $_proyecto, $_rol = '')
    {
        if ($_sigla == 'I') {
            return $this->_TRANS("INSERT INTO seg005(usuario, proyecto, rol) VALUES ('{$_id}','{$_proyecto}','{$_rol}'); ");
        } elseif ($_sigla == 'E') {
            return $this->_TRANS("DELETE FROM seg005 WHERE proyecto='{$_proyecto}' AND usuario='{$_id}'");
        }
    }


    function UsuarioPermiso($_permisos)
    {
        $sessionDatos = $this->SesionGet();
        $permisosStr = "";
        for ($i = 0; $i < count($_permisos); $i++) {
            if ($sessionDatos['PRL'] == substr($_permisos[$i], 0, 1)) {
                $permisosStr .= "'{$_permisos[$i]}',";
            }
        }
        //remover ultima coma
        $permisosStr = substr($permisosStr, 0, strlen($permisosStr) - 1);
        $str = "SELECT permiso,acceso FROM seguper WHERE usuario='{$sessionDatos['UID']}' AND permiso IN({$permisosStr})";
        $permiso = $this->_QUERY($sessionDatos['PRY'] == '' ? $str : $str .= " UNION SELECT permiso,acceso FROM seguperproy WHERE usuario='{$sessionDatos['UID']}' AND proyecto='{$sessionDatos['PRY']}' AND permiso IN({$permisosStr})");
        return $permiso;
    }

    function validaUsuarioPermiso($_permisos, $_acc = '')
    {
        return;
        /*    $datos = $this->UsuarioPermiso($_permisos);
          if ($datos) {
          //tiene acceso si es accion vacia o escritura/insercion/lectura o lectura/modificacion
          if ($_acc == '' || $datos[0]['acceso'] == 'E' || ($datos[0]['acceso'] == 'L' && $_acc == 'M')) {
          return;
          }
          }
          return header('location: ../seguridad/err2.php'); */
    }

    function UsuarioValidaCorreo($_usuario)
    {
        $A = $this->_QUERY("SELECT correo FROM seg001 WHERE id = '{$_usuario}';");
        $correo = explode('@', $A[0]['correo']);
        if ($correo[1] == 'una.ac.cr')
            return 1;
        else
            return 0;
    }

    /**
     * @param string $_cedula
     * @return array
     */
    function getUsuarioCedula($_cedula, $_tk = 0)
    {
        if ($_tk == 0) {
            return $this->_QUERY("SELECT id, CONCAT(nombre,' ', apellido1, ' ', apellido2) AS  nombre,correo,cedula FROM seg001 WHERE cedula='{$_cedula}'; ");
        } elseif ($_tk == 1) {
            return $this->_QUERY("SELECT cedula, nombre, apellido1, apellido2, correo, clave, tipo, estado FROM seg001 WHERE id='{$_cedula}'; ");
        }
    }

    function getUsuarioNombre($_nombre)
    {
        return $this->_QUERY("SELECT id, cedula, CONCAT(nombre,' ', apellido1,' ', apellido2) as nombre,correo FROM seg001 WHERE nombre LIKE '%{$_nombre}%' OR apellido1 LIKE'%{$_nombre}%' OR apellido2 LIKE '%{$_nombre}%'; ");
    }

    /**
     * @param string $_campo campo
     * @return int Consecutivo
     */
    function getConsecutivo($_campo)
    {
        $cs = $this->_QUERY("SELECT {$_campo} FROM sigespro");
        $this->_TRANS("UPDATE sigespro SET {$_campo}={$_campo} + 1;");
        return $cs[0][$_campo];
    }


    function modulo_get($_id = '')
    {
        $str = "SELECT id, descripcion, estado FROM segmod WHERE 1=1 ";
        $filtro = false;
        if ($_id != '') {
            $str .= "AND id='{$_id}' ";
            $filtro = true;
        }
        $str .= $filtro ? "" : "AND estado='1' ";
        return $this->_QUERY($str);
    }

    function permiso_get($_id, $_descripcion, $_modulo, $_perfil)
    {
        $str = "SELECT id, descripcion, modulo, perfil FROM segper WHERE 1=1 ";
        if ($_id != '') {
            $str .= "AND id='{$_id}' ";
        }
        if ($_descripcion != '') {
            $str .= "AND descripcion LIKE'%{$_descripcion}%' ";
        }
        if ($_modulo != '') {
            $str .= "AND modulo='{$_modulo}' ";
        }
        if ($_perfil != '') {
            $str .= "AND perfil='{$_perfil}' ";
        }
        return $this->_QUERY($str);
    }

    function usuario_permiso_get($_usuario, $_permiso, $_usuarioget = false)
    {
        $str = "SELECT seguper.usuario, seguper.permiso,seguper.acceso ";
        $str .= $_usuarioget ? ",CONCAT(seg001.nombre,' ',seg001.apellido1,' ',seg001.apellido2) AS nombreusuario,seg001.cedula FROM seguper INNER JOIN seg001 ON seguper.usuario = seg001.id WHERE 1=1 " : "FROM seguper WHERE 1=1 ";
        if ($_permiso != '') {
            $str .= "AND seguper.permiso='{$_permiso}' ";
        }
        return $this->_QUERY($str);
    }

    function usuario_permiso_IME($_accion, $_usuario, $_permiso, $_acceso = 'E')
    {
        if ($_accion == 'I') {
            $str = "INSERT INTO seguper(usuario, permiso,acceso) VALUES ('{$_usuario}','{$_permiso}','{$_acceso}')";
        } elseif ($_accion == 'E') {
            $str = "DELETE FROM seguper WHERE usuario='{$_usuario}' AND permiso='{$_permiso}'";
        } elseif ($_accion == 'M') {
            $str = "UPDATE seguper SET acceso='{$_acceso}' WHERE usuario='{$_usuario}' AND permiso='{$_permiso}'";
        }
        return $this->_TRANS($str);
    }

    // obtiene los usuarios sin el permiso asignado
    function usuario_permiso_calcula($_permiso)
    {
        return $this->_QUERY("SELECT id,CONCAT(seg001.nombre,' ',seg001.apellido1,' ',seg001.apellido2) AS nombreusuario,seg001.cedula FROM seg001 WHERE id NOT IN(SELECT usuario FROM seguper WHERE permiso='{$_permiso}');");
    }

    // obtiene los usuarios sin el permiso asignado
    function usuario_permiso_proyecto_calcula($_permiso, $_proyecto)
    {
        return $this->_QUERY("SELECT seg001.id,CONCAT(seg001.nombre,' ',seg001.apellido1,' ',seg001.apellido2) AS nombreusuario,seg001.cedula,seg005.rol FROM seg001 INNER JOIN seg005 ON seg005.usuario = seg001.id WHERE seg001.id IN (SELECT usuario FROM seg005 WHERE proyecto='{$_proyecto}') AND seg001.id NOT IN(SELECT usuario FROM seguperproy WHERE permiso='{$_permiso}' AND proyecto='{$_proyecto}') AND seg005.proyecto='{$_proyecto}'");
    }


}
