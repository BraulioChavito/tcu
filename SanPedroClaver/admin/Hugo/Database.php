<?php

/* * *****************************************************************
  CLASE DE NIVEL 0 UTILIZADA PARA CONEXION A LA BD
  (abstract SOLO PUEDE SER HEREDADA Y NO INSTANCIADA)
 * ***************************************************************** */

namespace Hugo;

use Hugo\PHPMailer\Exception;
use Hugo\PHPMailer\PHPMailer;

require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';

abstract class Database
{

    // Datos de coneccion
    private $datos;

    function __construct()
    {
        $this->datos = Config::getInstance()->getConfig()['db_mysql'];
    }

    /* METODOS FINAL NO PUEDEN SER REDEFINIDOS */

    final public function Rand($_tk = 20)
    {
        $letras = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $tam = strlen($letras);
        $str = '';
        for ($i = 0; $i < $_tk; $i++) {
            $str .= $letras[rand(0, $tam - 1)];
        }
        return $str;
    }


    final public function formatoMoneda($_moneda)
    {
        return number_format($_moneda, 2, '.', ',');
    }

    final public function _QUERY($_SQL)
    {
        $id_con = mysqli_connect($this->datos['host'], $this->datos['user'], $this->datos['password'], $this->datos['database']);
        $resultado = mysqli_query($id_con, $_SQL);
        $x = 0;
        $a = array();
        while ($row = mysqli_fetch_assoc($resultado)) {
            $a[$x] = $row;
            $x++;
        }
        mysqli_close($id_con);
        return $a;
    }

    function _TRANS($_SQL)
    {
        $id_con = mysqli_connect($this->datos['host'], $this->datos['user'], $this->datos['password'], $this->datos['database']);
        $resultado = mysqli_query($id_con, $_SQL);
        mysqli_close($id_con);
        return $resultado;
    }

    final public function formatoMonto($_monto)
    {
        return number_format($_monto, 2, ',', '');
    }

    final public function formatoMonto2($_monto)
    {
        return number_format($_monto, 2, '.', ',');
    }


    final public function FreeSQL($_VAR)
    {
        $v1 = array('"', "'", '´', '`', "#");
        $v2 = array('INSERT', "DELETE", 'SELECT', 'UPDATE', "DROP", "ALTER", "EXEC");
        $encontrado = false;
        foreach ($_VAR as $key => $value) {
            $value = str_replace($v1, ' ', $value);
            foreach ($v2 as $dato) {
                $pos = strpos($value, $dato);
                if ($pos === false) {

                } else {
                    $encontrado = true;
                    break;
                }
            }
        }
        if ($encontrado) {
            return true;
        } else {
            return false;
        }
    }

    final public function Fecha($_VAR, $_char = '-')
    {
        $_VAR = explode($_char, $_VAR);
        $_VAR = $_VAR[2] . $_char . $_VAR[1] . $_char . $_VAR[0];
        return $_VAR;
    }

    final public function FechaHora($_VAR, $_char = '-')
    {
        $_VAR = explode(' ', $_VAR);
        $FECHA = explode($_char, $_VAR[0]);
        $FECHA = $FECHA[2] . $_char . $FECHA[1] . $_char . $FECHA[0];
        return $FECHA . ' ' . $_VAR[1];
    }

    final public function Free($_VAR)
    {
        //SE USA CON LOS GET/POST
        $v1 = array('"', "'", '´', '`', "#");
        $_VAR = str_replace($v1, '', $_VAR);
        return $_VAR;
    }

    final public function FreeAcento($_VAR)
    {
        //ELIMINA LOS ACENTOS
        return preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml|caron);~i', '$1', htmlentities($_VAR, ENT_COMPAT, 'UTF-8'));
    }

    final public function FreePost($_VAR)
    {
        //SE USA CON LOS GET/POST
        $v1 = array('"', "'", '´', '`', "#");
        $_VAR = str_replace($v1, '', $_VAR);
        return $_VAR;
    }

    final public function _JQUERY($_SQL)
    {
        return json_encode($this->_QUERY($_SQL));
    }


    final public function Email($_destino, $_msj)
    {
        $destino = explode(',', $_destino);
        $mail = new PHPMailer(true);
        try {
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'braulioutn@gmail.com';
            $mail->Password = 'Saturno12345';
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;

            $mail->setFrom('braulioutn@gmail.com', 'Hogar san pedro claver');

            if (count($destino) > 1) {
                foreach ($destino as $dato) {
                    if ($dato != '') {
                        $mail->addAddress($dato);
                    }
                }
            } else {
                $mail->addAddress($_destino);
            }
            $mail->isHTML(true);

            $mail->Subject = 'Notificacion de sistema';
            $mail->Body = $_msj;

            $mail->send();
            return 1;
        } catch (Exception $e) {
            return -2;
        }
    }


}
