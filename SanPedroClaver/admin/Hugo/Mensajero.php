<?php

namespace Hugo;

abstract class Mensajero
{
    //constantes
    const __JQUERY__ = '../../Paco/js/';
    const __SWEET__ = '../../Paco/js/';
    const __SHELL__ = '../../Paco/shell/';
    const __JS__ = '../../Paco/js/';
    const __ESTILO__ = '../../Paco/CSS/';
    const __NAME__ = 'San Pedro Claver';
    const __ROOT__ = 'SanPedroClaver/Hugo/Seguridad/';
    const __IMAGENES__ = '../../Paco/multi/';


    function __construct()
    {
        $this->_Seguridad = new Seguridad();

        if (!$this->_Seguridad->SesionAuth()) {
            $this->Err();
        }
        $this->_Sesion = $this->_Seguridad->SesionGet();
        if (!$this->_Seguridad->SesionValida($this->_Sesion['_SPC_'], $this->_Sesion['UID'])) {
            $this->Portero();
        }
    }

    final function get_Sesion()
    {
        return $this->_Sesion;
    }

    final public function Err()
    {
        $ruta = 'http://www.hogarsanpedroclavercr.com/' . $this->_IP . '/' . self::__ROOT__;
        header("location: {$ruta}.error.php");
        exit;
    }


    final public function Portero()
    {
        //ESCOLTA A LA SALIDA DESDE CADA PAGINA
        $ruta = 'http://' . $this->_IP . '/' . self::__ROOT__;
        header("location: {$ruta}error3.php");
        exit;
    }


    final public function Incluir($_nombre, $_tipo, $_version = 0)
    {
        $_nombre = str_replace(' ', '', strtolower($_nombre));
        $_tipo = str_replace(' ', '', strtolower($_tipo));


        if ($_tipo == 'ajax') {
            /********** JQUERY **********/
            $ruta = self::__JQUERY__ . 'jquery-3.6.0.min.js';
            $ruta2 = self::__SWEET__ . 'sweetalert.min.js';

            file_exists($ruta) or die("Error archivo 'jquery-1.9.1.min.js' no encontrado");
            file_exists($ruta2) or die("Error archivo 'sweetalert.min.js' no encontrado");
            echo "<script type='text/javascript' language='javascript' src='{$ruta}?version={$_version}'></script>
                <script type='text/javascript' language='javascript' src='{$ruta2}'></script>";
            /********** PAGINA SHELL **********/
            $ruta = self::__SHELL__ . $_nombre . '/_' . $_version;
            file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
            echo "<script type='text/javascript' language='javascript'>var __SHELL__ = '{$ruta}'</script>";
            /********** JS DE PAGINA **********/
            $ruta = self::__JS__ . $_nombre . '/' . $_version . '.js';
            file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
            $token = date('dmYh:i:s');
            echo "<script type='text/javascript' language='javascript' src='{$ruta}?tk={$token}&version={$_version}'></script>";
        } elseif ($_tipo == 'js') {
            $ruta = self::__JS__ . $_nombre . '.js';
            file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
            if ($_nombre == 'window') {
                /********** SI LLAMAMOS AL window.js LLAMAMOS DE UNA VEZ A window.css **********/
                $ruta2 = self::__ESTILO__ . 'window.css';
                file_exists($ruta2) or die("Error archivo '{$ruta2}' no encontrado");
                echo "<link rel='stylesheet' type='text/css' href='{$ruta2}?version={$_version}'>
				<script type='text/javascript' language='javascript' src='{$ruta}?version={$_version}'></script>";
            } elseif ($_nombre == 'calendario') {
                /********** SI LLAMAMOS AL calendario.js LLAMAMOS DE UNA VEZ A LAS IMAGENES DE CALENDARIO **********/
                $ruta2 = self::__IMAGENES__ . '/calendario/';
                echo "<script type='text/javascript' language='javascript'>var ruta = '{$ruta2}'</script>
				<script type='text/javascript' language='javascript' src='{$ruta}?version={$_version}'></script>";
            } else
                echo "<script type='text/javascript' language='javascript' src='{$ruta}?version={$_version}'></script>";
         } elseif($_tipo == 'bkg'){
            /********** IMAGEN PARA BACKGROUND **********/
            $ruta = self::__IMAGENES__.$_nombre.'.png';
            file_exists($ruta) or die("Error archivo '{$ruta}' no encontrado");
            echo $ruta;


    } else die('Error de tipo');
    }

    final public function Title()
    {
        return self::__NAME__;
    }
}

?>