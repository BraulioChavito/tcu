<?php

namespace Hugo;
/**
 * Clase Config [Mantiene los datos del archivo config.json cargados en memoria para se utilizados donde se le requiera]
 *
 */
final class Config
{

    // Contenedor Instancia de la clase
    private static $instance = NULL;
    //Archivo de configuracion
    private $config;
    // Datos de facturacion electronica
    private $fe;

    // Constructor privado, previene la creación de objetos vía new
    private function __construct()
    {
        $string = file_get_contents(self::FILECONFIG());
        $this->config = json_decode($string, true);
    }

    // Clone no permitido
    private function __clone()
    {

    }

    // Método singleton 
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new Config();
        }

        return self::$instance;
    }

    private static function FILECONFIG()
    {
        return dirname(__FILE__) . "/../../configuracion/config.json";
    }

    final function getConfig()
    {
        return $this->config;
    }

    final function getFe()
    {
        if (is_null($this->fe)) {
            $datos = $this->_QUERYMYSQL("SELECT fe_user as user, fe_pass as pass, fe_cert as cert, fe_pin as pin, fe_endPoint as endPoint, fe_token_url as token_url, fe_client_id as client_id FROM sigespro;");
            $datos[0]['cert'] = dirname(__FILE__) . "/../seiya/docs/certificados/" . $datos[0]['cert'];
            $this->fe = $datos[0];
        }
        return $this->fe;
    }

    final public function _QUERYMYSQL($_SQL)
    {
        $id_con = mysqli_connect($this->config['db_mysql']['host'], $this->config['db_mysql']['user'], $this->config['db_mysql']['password'], $this->config['db_mysql']['database']);
        $resultado = mysqli_query($id_con, $_SQL);
        $x = 0;
        $a = array();
        while ($row = mysqli_fetch_assoc($resultado)) {
            $a[$x] = $row;
            $x++;
        }
        mysqli_close($id_con);
        return $a;
    }

}
