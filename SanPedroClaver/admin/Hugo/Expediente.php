<?php

namespace Hugo;

final class Expediente extends Database
{
    function expedienteMuestra()
    {
        $row = $this->_QUERY("select id, cedula, nombre, estado from datospersonales order by id ");
        return $row;
    }

    function MantExpediente($_accion, $_cedula, $_nombre, $_fechaNacimiento, $_nacionalidad, $_estadoCivil, $_escolaridad, $_ocupacion, $_direccion, $_fechaIngreso, $_estado)
    {
        $_cedula = $this->Free($_cedula);
        $_nombre = $this->Free($_nombre);
        $_fechaNacimiento = $this->Free($_fechaNacimiento);
        $_nacionalidad = $this->Free($_nacionalidad);
        $_estadoCivil = $this->Free($_estadoCivil);
        $_escolaridad = $this->Free($_escolaridad);
        $_ocupacion = $this->Free($_ocupacion);
        $_direccion = $this->Free($_direccion);
        $_fechaIngreso = $this->Free($_fechaIngreso);
        $_estado = $this->Free($_estado);


        if ($_accion == 'I') {
            $this->_TRANS("INSERT INTO datospersonales VALUES(' ','{$_cedula}','{$_nombre}', '{$_fechaNacimiento}','{$_nacionalidad}', '{$_estadoCivil}','{$_escolaridad}', '{$_ocupacion}','{$_direccion}','{$_fechaIngreso}','{$_estado}')");
            echo '1';
        }

        if ($_accion == 'M') {
            $this->_TRANS("UPDATE datospersonales SET cedula = '{$_cedula}',  nombre = '{$_nombre}', fechaNacimiento = '{$_fechaNacimiento}',
            nacionalidad ='{$_nacionalidad}', estadoCivil = '{$_estadoCivil}', escolaridad = '{$_escolaridad}', ocupacion = '{$_ocupacion}', direccion = '{$_direccion}' ,
            fechaIngreso = '{$_fechaIngreso}', estado = '{$_estado}' where cedula ='{$_cedula}'");
            echo '10';
        }
    }


    function MantContacto($_id, $_accion, $_cedulaPAM, $_cedulaContacto, $_nombreContacto, $_direccioncontacto, $_parentesco, $_ocupacion, $_email, $_telefono)
    {
        $_id = $this->Free($_id);
        $_cedulaPAM = $this->Free($_cedulaPAM);
        $_cedulaContacto = $this->Free($_cedulaContacto);
        $_nombreContacto = $this->Free($_nombreContacto);
        $_direccioncontacto = $this->Free($_direccioncontacto);
        $_parentesco = $this->Free($_parentesco);
        $_email = $this->Free($_email);
        $_telefono = $this->Free($_telefono);


        if ($_accion == 'I') {
            $this->_TRANS("INSERT INTO contacto VALUES('{$_id}','{$_cedulaPAM}','{$_cedulaContacto}', '{$_nombreContacto}','{$_direccioncontacto}', '{$_parentesco}','{$_email}', '{$_ocupacion}','{$_telefono}', 1)");
            echo '10';
        }

        if ($_accion == 'M') {

            $this->_TRANS("UPDATE contacto SET estado = 0 where (id={$_id})");
            $this->_TRANS("INSERT INTO contacto VALUES('{$_id}','{$_cedulaPAM}','{$_cedulaContacto}', '{$_nombreContacto}','{$_direccioncontacto}', '{$_parentesco}','{$_email}', '{$_ocupacion}','{$_telefono}', 1)");

            echo '10';
            /* $this->_TRANS("UPDATE datospersonales SET cedula = '{$_cedula}',  nombre = '{$_nombre}', fechaNacimiento = '{$_fechaNacimiento}',
             nacionalidad ='{$_nacionalidad}', estadoCivil = '{$_estadoCivil}', escolaridad = '{$_escolaridad}', ocupacion = '{$_ocupacion}', direccion = '{$_direccion}' ,
             fechaIngreso = '{$_fechaIngreso}', estado = '{$_estado}' where cedula ='{$_cedula}'");
             echo '10';*/
        }
    }

    function MantMedico($_id, $_accion, $_cedulaPAM, $_discapacidad, $_padecimientos, $_cirugias, $_ayudas, $_medicamentos, $_alergias, $_observaciones)
    {
        $_id = $this->Free($_id);
        $_cedulaPAM = $this->Free($_cedulaPAM);
        $_discapacidad = $this->Free($_discapacidad);
        $_padecimientos = $this->Free($_padecimientos);
        $_cirugias = $this->Free($_cirugias);
        $_ayudas = $this->Free($_ayudas);
        $_medicamentos = $this->Free($_medicamentos);
        $_alergias = $this->Free($_alergias);
        $_observaciones = $this->Free($_observaciones);


        if ($_accion == 'I') {
            $this->_TRANS("INSERT INTO medico VALUES('{$_id}','{$_cedulaPAM}','{$_discapacidad}', '{$_padecimientos}','{$_cirugias}', '{$_ayudas}','{$_medicamentos}', '{$_alergias}','{$_observaciones}')");
            echo '10';
        }

        if ($_accion == 'M') {
            $this->_TRANS("UPDATE MEDICO SET DISCAPACIDAD = '{$_discapacidad}', padecimientos = '{$_padecimientos}', cirugias = '{$_cirugias}', ayudas = '{$_ayudas}', medicamentos = '{$_medicamentos}', alergias = '{$_alergias}', observaciones = '{$_observaciones}'");

            echo '10';
            /* $this->_TRANS("UPDATE datospersonales SET cedula = '{$_cedula}',  nombre = '{$_nombre}', fechaNacimiento = '{$_fechaNacimiento}',
             nacionalidad ='{$_nacionalidad}', estadoCivil = '{$_estadoCivil}', escolaridad = '{$_escolaridad}', ocupacion = '{$_ocupacion}', direccion = '{$_direccion}' ,
             fechaIngreso = '{$_fechaIngreso}', estado = '{$_estado}' where cedula ='{$_cedula}'");
             echo '10';*/
        }
    }


    function medicoVacio()
    {
        return array(0 => array(
            'id' => '-1',
            'cedulaPAM' => '',
            'discapacidad' => '',
            'padecimientos' => '',
            'cirugias' => '',
            'ayudas' => '',
            'medicamentos' => '',
            'alergias' => '',
            'observaciones' => ''
        ));
    }


    function ExpedienteVacio()
    {
        return array(0 => array(
            'id' => '-1',
            'cedula' => '',
            'nombre' => '',
            'fechaNacimiento' => '',
            'nacionalidad' => '',
            'estadoCivil' => '',
            'escolaridad' => '',
            'ocupacion' => '',
            'direccion' => '',
            'fechaIngreso' => '',
            'estado' => ''
        ));
    }


    function contactoVacio()
    {

        return array(0 => array(
            'id' => '-1',
            'cedula' => '',
            'nombre' => '',
            'cedulaContacto' => '',
            'nombreContacto' => '',
            'direccioncontacto' => '',
            'parentesco' => '',
            'ocupacion' => '',
            'email' => '',
            'telefono' => '',
            'estado' => ''
        ));
    }


    function ExpedienteDetalle($_id)
    {
        $ROW = $this->_QUERY("SELECT {$_id} as id, cedula, nombre, fechaNacimiento, nacionalidad, estadocivil, escolaridad,
       ocupacion, direccion, fechaingreso, estado from datospersonales where (id={$_id});");
        return $ROW;
    }

    function contactoDetalle($_id)
    {
        $ROW = $this->_QUERY("SELECT {$_id} as id, cedulaC as cedulaContacto, nombreC as nombreContacto, direccionC
    as direccionContacto, parentezcoC as parentesco, ocupacionC as ocupacion, emailC as email, telefonoC as telefono, estado
       from contacto where (id={$_id}) and estado = 1;");
        return $ROW;
    }

    function medicoDetalle($_id)
    {
        $ROW = $this->_QUERY("SELECT {$_id} as id, discapacidad, padecimientos, cirugias, ayudas, medicamentos,
    alergias, observaciones from medico where (id = {$_id});");
        return $ROW;
    }


}