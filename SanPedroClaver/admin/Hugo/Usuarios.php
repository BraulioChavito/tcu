<?php


namespace Hugo;

final class Usuarios extends Database
{

    function UsuarioMuestra()
    {
        return $this->_QUERY("select id, cedula, nombre, correo, telefono, estado from segusuarios order by id ");
    }

    function rolesMuestra()
    {
        return $this->_QUERY("select id_rol as id, descripcion_rol as nombre, estado_rol as estado from roles order by id ");
    }


    function UsuariosVacio()
    {
        return array(0 => array(
            'id' => '-1',
            'cedula' => '',
            'nombre' => '',
            'correo' => '',
            'telefono' => '',
            'clave' => '',
            'token' => '',
            'estado' => '',
            'actualiza' => ''
        ));
    }

    function sinRol(){
        return array(0 => array(
            'id_rol' => '-1',
            'descripcion_rol' => '',
            'estado_rol' => ''
        ));
    }

function rolDetalle($_id){
    $ROW = $this->_QUERY("SELECT {$_id} as id_rol, descripcion_rol as descripcion, estado_rol as estado from roles where (id_rol = {$_id} ); ");
    return  $ROW;
}




    function UsuariosDetalle($_id)
    {
        $ROW = $this->_QUERY("SELECT {$_id} AS id, segusuarios.cedula, segusuarios.nombre, segusuarios.correo, segusuarios.telefono, segusuarios.clave, segusuarios.estado, 
       roles.descripcion_rol as rol from segusuarios INNER join usuario_rol on segusuarios.id = usuario_rol.usuario inner join
        roles on usuario_rol.rol = roles.id_rol WHERE (id={$_id});");
        return $ROW;
    }
/*
    function UsuariosDetalleRol($_usuario)
    {
        $ROW1 = $this->_QUERY("SELECT usuario_rol.rol, usuario_rol.estado, rol.descripcion_rol as nombre from usuario_rol inner join rol on usuario_rol.rol = rol.id_rol WHERE (usuario = {$_usuario});");
        return $ROW1;
    }
*/
    function ListarRoles()
    {
        $ROW = $this->_QUERY("SELECT id_rol as id_rol ,descripcion_rol as nombre from roles WHERE (estado_rol = 1);");
        return $ROW;
    }

    function MantUsuarios($_accion, $_cedula, $_nombre, $_correo, $_telefono, $_clave, $_estado, $_rol)
    {
        $_cedula = $this->Free($_cedula);
        $_nombre = $this->Free($_nombre);
        $_correo = $this->Free($_correo);
        $_telefono = $this->Free($_telefono);
        $_estado = $this->Free($_estado);
        $_rol = $this->Free($_rol);

        if ($_accion == 'I') {
            //revisa sui ya existe, de exisistir devuelve error


            $_clave = base64_encode($_clave);
            $this->_TRANS("INSERT INTO SEGUSUARIOS VALUES(' ','{$_cedula}','{$_nombre}', '{$_correo}','{$_telefono}', '{$_clave}',0,'{$_estado}',1 )");
            //consulta el id recien ingresado
            $ROW = $this->_QUERY("SELECT id as id from segusuarios WHERE cedula = '{$_cedula}'");
          //guarda el rol
            $con = $ROW[0]['id'];
            $this->_TRANS("INSERT INTO usuario_rol VALUES( '{$con}','{$_rol}','{$_estado}')");
            echo '1';
        }

        if ($_accion == 'M') {
            $this->_TRANS("UPDATE SEGUSUARIOS SET nombre = '{$_nombre}', correo = '{$_correo}', telefono ='{$_telefono}', clave = '{$_clave}', estado = '{$_estado}' where cedula ='{$_cedula}'");
            echo '10';
        }

        if ($_accion == 'E') {
            $this->_TRANS("UPDATE SEGUSUARIOS SET estado = 0 where cedula ='{$_cedula}' )");
            echo '1';
        }
    }

    function mantRoles($_id, $_accion,  $_descripcion, $_estado){
        $_estado = $this->Free($_estado);
        $_descripcion = $this->Free($_descripcion);

        if ($_accion == 'I') {
            $this->_TRANS("INSERT INTO roles VALUES( ' ','{$_descripcion}','{$_estado}')");
            echo '1';
        }

        if ($_accion == 'M') {
            $this->_TRANS("UPDATE roles SET descripcion_rol = '{$_descripcion}', estado_rol = '{$_estado}' where id_rol = '{$_id}'");
            echo '1';
        }


    }


}