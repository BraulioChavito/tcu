<?php

use Hugo\Mensajero;
use Hugo\Seguridad;
use Hugo\Usuarios;

spl_autoload_register(function ($_BaseClass) {
    require_once dirname(__FILE__) . "/../../../{$_BaseClass}.php";
});

$Usuarios = new Usuarios();
if (isset($_POST['_AJAX'])) {
    if ($_POST['_AJAX'] == 1) {


        $Usuarios->mantRoles($_POST['ID'],$_POST['accion'], $_POST['descripcion'], $_POST['estado']);
    }


} else {



         class _rolper_Detalle extends Mensajero
        {




            private $_ROW = array();
            private $Securitor = '';
            private $User = '';

            function __construct()
            {
                $this->User = new Usuarios();
                $this->Securitor = new Seguridad();
                if (!$this->Securitor->SesionAuth()) $this->Err();
                $this->_ROW = $this->Securitor->SesionGet();
            }

            function Get($_campo)
            {
                return $this->_ROW[$_campo];
            }

            function rol($_rol)
            {
                return $this->Securitor->getRol($_rol);
            }

             function ObtieneDatos(){
                 if (!isset($_GET['ID']) or $_GET['ID'] == '')
                     return $this->User->sinRol();
                 else
                     return $this->User->rolDetalle($_GET['ID']);
             }

        }

}