<?php

use Hugo\Mensajero;
use Hugo\Seguridad;
use Hugo\Usuarios;

spl_autoload_register(function ($_BaseClass) {
    require_once dirname(__FILE__) ."/../../../{$_BaseClass}.php";
});
final class _rolper extends Mensajero {

    private $_ROW = array();
    private $Securitor = '';
    private $User = '';

    function __construct()
    {
        $this->User = new Usuarios();
        $this->Securitor = new Seguridad();
        if (!$this->Securitor->SesionAuth()) $this->Err();
        $this->_ROW = $this->Securitor->SesionGet();

    }

    function Get($_campo)
    {
        return $this->_ROW[$_campo];
    }

    function rolesMuestra()
    {
        return $this->User->rolesMuestra();
    }

    function rol($_rol)
    {
        return $this->Securitor->getRol($_rol);
    }

}