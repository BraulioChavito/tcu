<?php

use Hugo\Mensajero;
use Hugo\Seguridad;
use Hugo\Usuarios;

spl_autoload_register(function ($_BaseClass) {
    require_once dirname(__FILE__) . "/../../../{$_BaseClass}.php";
});

$Usuarios = new Usuarios();
if (isset($_POST['_AJAX'])) {
    if ($_POST['_AJAX'] == 1) {
        $Usuarios->mantRoles('',$_POST['accion'], $_POST['descripcion'], $_POST['estado']);
    }


} else {


    final class _crear_rolper extends Mensajero
    {
        private $_ROW = array();
        private $Securitor = '';
        private $User = '';

        function __construct()
        {
            $this->User = new Usuarios();
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();
        }


    }
}