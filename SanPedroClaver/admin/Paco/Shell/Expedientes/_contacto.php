<?php

use Hugo\Mensajero;
use Hugo\Seguridad;
use Hugo\Usuarios;
use Hugo\Expediente;


spl_autoload_register(function ($_BaseClass) {
    require_once dirname(__FILE__) . "/../../../{$_BaseClass}.php";
});


$Expediente = new Expediente();

if (isset($_POST['_AJAX'])) {
    if ($_POST['_AJAX'] == 1) {
        $Expediente->MantContacto( $_POST['id'], $_POST['accion'], $_POST['cedulaPAM'], $_POST['cedulaContacto'],
            $_POST['nombreContacto'], $_POST['direccioncontacto'], $_POST['parentesco'], $_POST['ocupacion'], $_POST['email'], $_POST['telefono']);
    }

} else {
    spl_autoload_register(function ($_BaseClass) {
        require_once dirname(__FILE__) . "/../../../{$_BaseClass}.php";
    });

    final class _contacto extends Mensajero
    {
        private $_ROW = array();
        private $Securitor = '';
        private $User = '';
        private $File = '';


        function __construct()
        {
            $this->File = new Expediente();
            $this->User = new Usuarios();
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();

        }

        function ObtieneDatos()
        {
            if (!isset($_GET['ID']) or $_GET['ID'] == '') {
                return $this->File->contactoVacio();
            } else {
                return $this->File->contactoDetalle($_GET['ID']);
            }
        }

        function ObtieneDatosPAM()
        {
            if (!isset($_GET['ID']) or $_GET['ID'] == '') {
                return $this->File->ExpedienteVacio();
            } else {
                return $this->File->ExpedienteDetalle($_GET['ID']);
            }
        }

    }
}
