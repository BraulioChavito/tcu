<?php

use Hugo\Mensajero;
use Hugo\Seguridad;
use Hugo\Usuarios;
use Hugo\Expediente;


spl_autoload_register(function ($_BaseClass) {
    require_once dirname(__FILE__) . "/../../../{$_BaseClass}.php";
});

$Expediente = new Expediente();

if (isset($_POST['_AJAX'])) {
    if ($_POST['_AJAX'] == 1) {
        $Expediente->MantMedico($_POST['id'], $_POST['accion'], $_POST['cedulaPAM'], $_POST['discapacidad'],
            $_POST['padecimientos'], $_POST['cirugias'], $_POST['ayudas'], $_POST['medicamentos'], $_POST['alergias'], $_POST['observaciones']);
    }

} else {
    spl_autoload_register(function ($_BaseClass) {
        require_once dirname(__FILE__) . "/../../../{$_BaseClass}.php";
    });


    final class _medico extends Mensajero
    {
        private $_ROW = array();
        private $Securitor = '';
        private $User = '';
        private $File = '';


        function __construct()
        {
            $this->File = new Expediente();
            $this->User = new Usuarios();
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();

        }

        function ObtieneDatosPAM()
        {
            if (!isset($_GET['ID']) or $_GET['ID'] == '') {
                return $this->File->ExpedienteVacio();
            } else {
                return $this->File->ExpedienteDetalle($_GET['ID']);
            }
        }

        function ObtieneDatos()
        {
            if (!isset($_GET['ID']) or $_GET['ID'] == '') {
                return $this->File->medicoVacio();
            } else {
                return $this->File->medicoDetalle($_GET['ID']);
            }
        }

    }
}