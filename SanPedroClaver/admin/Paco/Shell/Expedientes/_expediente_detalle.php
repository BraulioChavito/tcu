<?php

use Hugo\Mensajero;
use Hugo\Seguridad;
use Hugo\Usuarios;
use Hugo\Expediente;


spl_autoload_register(function ($_BaseClass) {
    require_once dirname(__FILE__) . "/../../../{$_BaseClass}.php";
});

$Expediente = new Expediente();


if (isset($_POST['_AJAX'])) {
    if ($_POST['_AJAX'] == 1) {
        $Expediente->MantExpediente($_POST['accion'], $_POST['cedula'], $_POST['nombre'], $_POST['fechaNacimiento'], $_POST['nacionalidad'], $_POST['estadoCivil'], $_POST['escolaridad'], $_POST['ocupacion'], $_POST['direccion'], $_POST['fechaIngreso'], $_POST['estado']);
    }

} else {
    spl_autoload_register(function ($_BaseClass) {
        require_once dirname(__FILE__) . "/../../../{$_BaseClass}.php";
    });

    final class _expediente_detalle extends Mensajero
    {
        private $_ROW = array();
        private $Securitor = '';
        private $User = '';
        private $File = '';


        function __construct()
        {
            $this->File = new Expediente();
            $this->User = new Usuarios();
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();

        }

        function Get($_campo)
        {
            return $this->_ROW[$_campo];
        }

        function rol($_rol)
        {
            return $this->Securitor->getRol($_rol);
        }

        function ObtieneDatos()
        {
            if (!isset($_GET['ID']) or $_GET['ID'] == '') {
                return $this->File->ExpedienteVacio();
            } else {
                return $this->File->ExpedienteDetalle($_GET['ID']);
            }
        }

    }
}


?>