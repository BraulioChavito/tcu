<?php


use Hugo\Mensajero;
use Hugo\Seguridad;
use Hugo\Usuarios;

if( isset($_POST['_AJAX'])) {
    function spl_autoload_register($_BaseClass){require_once "../../../../{$_BaseClass}.php";}

        if( !isset($_POST['id']) ) die('-1');

        $Securitor = new Seguridad();
        if(!$Securitor ->SesionAuth()) die('-0');
        $_ROW = $Securitor->SesionGet();

        exit;

} else {
    spl_autoload_register(function ($_BaseClass) {
        require_once dirname(__FILE__) ."/../../../{$_BaseClass}.php";
    });


    final class _usuarios extends Mensajero
    {
        private $_ROW = array();
        private $Securitor = '';
        private $User = '';

        function __construct()
        {
            $this->User = new Usuarios();
            $this->Securitor = new Seguridad();
            if (!$this->Securitor->SesionAuth()) $this->Err();
            $this->_ROW = $this->Securitor->SesionGet();

        }

        function Get($_campo)
        {
            return $this->_ROW[$_campo];
        }

        function rol($_rol)
        {
            return $this->Securitor->getRol($_rol);
        }

        function UsuariosMuestra()
        {
            return $this->User->UsuarioMuestra();
        }


    }
}

?>

