<?php

namespace Seguridad;

use Hugo\Mensajero;
use Hugo\Seguridad;

spl_autoload_register(function ($_BaseClass) {
    require_once dirname(__FILE__) . "/../../../{$_BaseClass}.php";
});

/*
if($_POST['_AJAX'] == '1'){
    error_log("estoy aqui..");
}
/*$ajax = $_POST['_AJAX'];
$user = $_POST['Usuarios'];
$clave = $_POST['Clave'];

*/

final class _principal extends Mensajero
{
    private $_ROW = array();
    private $Securitor = '';

    function __construct()
    {
        $this->Securitor = new Seguridad();
        if (!$this->Securitor->SesionAuth()) $this->Err();
        $this->_ROW = $this->Securitor->SesionGet();
        $_POST['_UMAIL_'] = $this->_ROW['UMAIL'];
        $_POST['_UNAME_'] = $this->_ROW['UNAME'];
        $_POST['_UID_'] = $this->_ROW['UID'];
        $_POST['_UCED_'] = $this->_ROW['UCED'];
    }

    function Get($_campo)
    {
        return $this->_ROW[$_campo];
    }

    function rol($_rol)
    {
        return $this->Securitor->getRol($_rol);
    }


}


?>