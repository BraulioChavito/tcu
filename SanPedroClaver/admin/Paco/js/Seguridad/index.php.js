function revisar() {

    var user = ($('#usuario').val());
    var clave = ($('#clave').val());

    if (user == '') {
        swal.fire("Digite un usuario");

    }
    if (clave == '') {
        swal.fire("Digite una Contraseña");
    }

    var parametros = {
        "_AJAX": 1,
        "usuario": $('#usuario').val(),
        "clave": $('#clave').val()
    };
    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            Swal.fire({
                title: "Por favor espere!",
                text: "Estoy validando los credenciales de acceso!",
                type: "info",
                timer: 5000,
                allowOutsideClick: false,
                showConfirmButton: false
            });
        },
        success: function (_response) {
            switch (_response) {
                case '-1':
                    alert("");
                    break;
                case '-2':
                    swal.fire('Alerta!', 'Usuarios y/o contraseña incorrecta.', 'warning');
                    break;
                /*  case '-3':
                      demo.showSwal('Error!', 'No se encontró al usuario en el sistema.', 'error');
                      break;
                  case '-4':
                      demo.showSwal('Alerta!', 'Su usuario está desactivado. Contacte con el administrador!', 'warning');
                      break;*/
                case '1':
                    Swal.fire({
                        title: "Sesión Iniciada!",
                        text: "Por favor espere...",
                        type: "success",
                        allowOutsideClick: false,
                        timer: 5000,
                        showConfirmButton: false
                    });
                    location.href = 'principal.php';
                    break;
                default:
                    Swal.fire('Error!', 'Tiempo de espera agotado.', 'error');
                    break;
            }
        }
    });

}




