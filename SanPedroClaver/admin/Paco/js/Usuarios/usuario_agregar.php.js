function cerrar() {
    window.close();
}

function datos(accion) {


    if ($('#cedula').val() == '') {
        alert('Debe indicar número de cédula');
        return;
    }
    if ($('#nombre').val() == '') {
        alert('Debe indicar un nombre');
        return;
    }
    if ($('#correo').val() == '') {
        alert('Debe indicar un correo valido');
        return;
    }
    if ($('#telefono').val() == '') {
        alert('Debe indicar un telefono');
        return;
    }
    if ($('#clave').val() == '') {
        alert('Debe indicar una clave');
        return;
    }

    if ($('#listaRoles').val() == '') {
        alert('Debe seleccionar un rol');
        return;
    }
    if ($('#estado').val() == '') {
        alert('Debe seleccionar un estado');
        return;
    }

    var parametros = {
        '_AJAX': 1,
        'id': $('#id').val(),
        'accion': accion,
        'cedula': $('#cedula').val(),
        'nombre': $('#nombre').val(),
        'correo': $('#correo').val(),
        'telefono': $('#telefono').val(),
        'clave': $('#clave').val(),
        'listaRoles': $('#listaRoles').val(),
        'estado': $('#estado').val(),
        'rol': $('#id_rol').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {

            switch (_response) {
                case '-2':
                    alert('Ya existe usuario');
                    break;
                case '-0':
                    alert('Sesión expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el envío de parámetros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    /*window.dialogArguments*/
                    opener.location.reload();
                    swal.fire({
                        title: 'Transaccion Finalizada',
                        text: "Se han guardado los datos",
                        type: 'save',
                        timer: 2000,
                    });
                    setTimeout("self.close()",2000)

                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
                //default:alert(_response);break;
            }
        }
    });


}