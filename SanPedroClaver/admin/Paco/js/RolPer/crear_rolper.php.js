function cerrar() {
    window.close();
}

function datos(accion) {
    if ($('#descripcion').val() == '') {
        alert('Debe indicar una descripcion');
        return;
    }
    if ($('#estado').val() == '') {
        alert('Debe seleccionar un estado');
        return;
    }

    var parametros = {
        '_AJAX': 1,
        'id': $('#id').val(),
        'accion': accion,
        'descripcion': $('#descripcion').val(),
        'estado': $('#estado').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {
            switch (_response) {
                case '-2':
                    alert('Ya existe usuario');
                    break;
                case '-0':
                    alert('Sesión expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el envío de parámetros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '1':
                    /*window.dialogArguments*/
                      opener.location.reload();
                    swal.fire({
                        title: 'Transaccion Finalizada',
                        text: "Se han guardado los datos",
                        type: 'save',
                        timer: 2000,
                    });
                    setTimeout("self.close()", 2000)
//                    location.href = "../../../../../SanPedroClaver/admin/Luis/Seguridad/principal.php"
                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
                //default:alert(_response);break;
            }
        }
    });


}