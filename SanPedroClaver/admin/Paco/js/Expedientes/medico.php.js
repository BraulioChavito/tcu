function cerrar() {
    window.close();
}


function datos(accion) {

     var parametros = {
        '_AJAX': 1,
        'id': $('#idPAM').val(),
        'accion': accion,
        'cedulaPAM': $('#cedulaPAM').val(),
        'discapacidad': $('#discapacidad').val(),
        'padecimientos': $('#padecimientos').val(),
        'cirugias': $('#cirugias').val(),
        'ayudas': $('#ayudas').val(),
        'medicamentos': $('#medicamentos').val(),
        'alergias': $('#alergias').val(),
        'observaciones': $('#observaciones').val()
    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {

            switch (_response) {
                case '-2':
                    alert('Ya existe usuario');
                    break;
                case '-0':
                    alert('Sesión expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el envío de parámetros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '10':
                    /*window.dialogArguments*/
                    // opener.location.reload();
                    swal.fire({
                        title: 'Transaccion Finalizada',
                        text: "Se han guardado los datos",
                        type: 'save',
                        timer: 2000,
                    });
                    setTimeout("self.close()",2000)

                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
                //default:alert(_response);break;
            }
        }
    });


}