function cerrar() {
    window.close();
}


function datos(accion) {


    if ($('#cedula').val() == '') {
        alert('Debe indicar número de cédula');
        return;
    }
    if ($('#nombre').val() == '') {
        alert('Debe indicar un nombre');
        return;
    }
    if ($('#fechaNacimiento').val() == '') {
        alert('Debe indicar una fecha de nacimiento');
        return;
    }
    if ($('#nacionalidad').val() == '') {
        alert('Debe indicar una nacionalidad');
        return;
    }
    if ($('#estadoCivil').val() < 1) {
        alert('Debe seleccionar un estado civil');
        return;
    }

    if ($('#escolaridad').val() == '') {
        alert('Debe seleccionar una escolaridad');
        return;
    }
    if ($('#ocupacion').val() == '') {
        alert('Debe digitar una ocupacion');
        return;
    }
    if ($('#direccion').val() == '') {
        alert('Debe ingresar una direccion');
        return;
    }
    if ($('#fechaIngreso').val() == '') {
        alert('Debe seleccionar una fecha de ingreso');
        return;
    }
    if ($('#estado').val() == '') {
        alert('Debe seleccionar un estado');
        return;
    }

    var parametros = {
        '_AJAX': 1,
        'id': $('#id').val(),
        'accion': accion,
        'cedula': $('#cedula').val(),
        'nombre': $('#nombre').val(),
        'fechaNacimiento': $('#fechaNacimiento').val(),
        'nacionalidad': $('#nacionalidad').val(),
        'estadoCivil': $('#estadoCivil').val(),
        'escolaridad': $('#escolaridad').val(),
        'ocupacion': $('#ocupacion').val(),
        'direccion': $('#direccion').val(),
        'fechaIngreso': $('#fechaIngreso').val(),
        'estado': $('#estado').val()

    };

    $.ajax({
        data: parametros,
        url: __SHELL__,
        type: 'post',
        beforeSend: function () {
            $('#btn').disabled = true;
            ALFA('Por favor espere....');
        },
        success: function (_response) {

            switch (_response) {
                case '-2':
                    alert('Ya existe usuario');
                    break;
                case '-0':
                    alert('Sesión expirada [Err:0]');
                    break;
                case '-1':
                    alert('Error en el envío de parámetros [Err:-1]');
                    break;
                case '0':
                    OMEGA('Error transaccional');
                    $('#btn').disabled = false;
                    break;
                case '10':
                    /*window.dialogArguments*/
                    opener.location.reload();
                    swal.fire({
                        title: 'Transaccion Finalizada',
                        text: "Se han guardado los datos",
                        type: 'save',
                        timer: 2000,
                    });
                    setTimeout("self.close()",2000)

                    break;
                default:
                    alert('Tiempo de espera agotado');
                    break;
                //default:alert(_response);break;
            }
        }
    });


}